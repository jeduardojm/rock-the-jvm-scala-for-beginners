package part1basics

object Functions extends App{

  def aFunction(a: String, b: String): String ={
    a + "" + b
  }

  println("hello",3)

  def aParameterlessFunction(): Int = 42
  println(aParameterlessFunction())
  println(aParameterlessFunction)

  case class example(a:String, b:Int)
  val listExample = List(example("Lalo",25),example("Dennis",26),example("Mari",27),example("Bubu",28))

  def aRepeatedFunction(lista:List[example]):String={

    def otherFilter(lista:List[example],one:String = ""):String = {
      var a = lista.head.a
      var b = lista.head.b
      var returnString:String = if (lista.size == 1) s"$a, tienes $b años?" else s"$a, tienes $b años? => "

      var otro = lista match {
        case _ if lista.size == 1 => one + returnString
        case x :: t2 => otherFilter(t2, one + returnString)
      }
      otro
    }
    otherFilter(lista)
  }

  println(aRepeatedFunction(listExample))

  // WHEN YOU NEED LOOPS, USE RECURSION

  def aFunctionWithSideEffects(aString: String): Unit = println(aString)

  def aBigFunction(n: Int):Int = {
    def aSmallerFunction(a:Int, b:Int):Int = a + b

    aSmallerFunction(n,n-1)
  }

  def greetingForKids(name:String,age:Int): String ={
    s"Hi, my name is $name and I am $age years old"
  }
  println(greetingForKids("David",12))

  def factorial(n: Int): Int = {
    if(n <= 0) 1
    else n * factorial(n -1)
  }

  println(factorial(5))


  def fibonnaci(n: Int): Int =
    if(n <= 1) 1
    else fibonnaci(n-1) + fibonnaci(n-2)

  println(fibonnaci(8))

  def isPrime(n: Int): Boolean ={
    def isPrimeUntil(t:Int): Boolean =
      if(t <= 1) true
      else n % t != 0 && isPrimeUntil(t -1)

     isPrimeUntil(n / 2)
  }

  println(isPrime(37))
  println(isPrime(2003))
  println(37 * 17)
}
