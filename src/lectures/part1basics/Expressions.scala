
package lectures.part1basics

object Expressions extends App{

  val x = 1 +2
  println(x)

  println(2 + 3 * 4)
  // + - * / & | ^ << >> >>>

  println(1 == x)
  // = != > >= < <=

  println(!(1 == x))
  // ! && ||

  var aVariable = 2
  aVariable += 3 // also works with -= *= /=
  println(aVariable)

  //Instructions (Doing also) vs Expressions (VALUE)

  //IF Expressions

  val aCondition = true
  val aConditionedValue = if(aCondition) 5 else 3
  println(aConditionedValue)

  var i = 0
  val aWhile = while(i < 10){
    println(i)
    i +=1
  }
  //EVERYTHING in scala is an expression!

  val aWeirdValue = (aVariable == 3)  // Unit === void
  println(aWeirdValue)

  val aCodeBlocks = {
    val y = 2
    val z = y + 1
    if(z > 2) "hello" else "goodbye"
  }

  //instructions are executed && expressions are evaluated

}
