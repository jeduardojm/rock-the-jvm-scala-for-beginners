package part1basics

object Recursion extends App{

  def factorial(n:Int):Int ={
    if (n <= 1) 1
    else {
      println(s"factorial:$n, I first need factorial of:${n-1}")
      val result = n * factorial(n-1)
      println(s"factorial:$n")
      result
    }
  }

 // println(factorial(10))
 // println(factorial(5000))

  def anotherFactorial(n: Int):BigInt = {
    def factHelper(x:Int, acc:BigInt):BigInt =
      if(x <= 1) acc
      else factHelper(x - 1, x * acc)

   factHelper(n,1)
  }

  println(anotherFactorial(5000))

  def concatenateTailRec(sString:String,n:Int,acc:String):String ={
    if(n<=0)acc
    else concatenateTailRec(sString,n-1, sString +acc)
  }

  def isPrime(n:Int):Boolean ={
    def isPrimeHelper(x:Int, z:Boolean):Boolean=
      if (!z)false
      else if (x <= 1) true
      else isPrimeHelper(x-1,n % x != 0 && z)

    isPrimeHelper(n/2,true)
  }

  println(isPrime(3))
  println(isPrime(4))
}
