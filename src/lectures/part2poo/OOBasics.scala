package part2poo

object OOBasics extends App {

  val person = new Person("Jonh", 26)

  person.greet("Daniel")

  val author = new Writer("Charles","Dickens",1812)
  val novel = new Novel("Great Expections", 1861, author)

  println(novel.authorAge)
  println(novel.isWritterBy(author))

  val counter = new Counter
  counter.inc.print
  counter.inc.inc.inc.print
  counter.inc(10).print

}

class Person(name:String,age:Int) {

  def greet(name:String) = println(s"${this.name} says: Hi, $name!")
}

class Writer(firstname:String,surname:String, val year:Int){

  def fullName() = firstname + " " + surname

}
class Novel(name:String, year:Int, author:Writer) {

  def authorAge = year - author.year
  def isWritterBy(author:Writer) = author == this.author
  def copy(newYear: Int): Novel = new Novel(name,newYear,author)
}

class Counter(val count: Int = 0){
  def inc = {
    println("Incrementing")
    new Counter(count + 1)
  }

  def dec = {
    println("decrementing")
    new Counter(count - 1)
  }

  def inc(n:Int):Counter = {
    if (n <= 0) this
    else inc.inc(n-1)
  }
  def dec(n:Int):Counter = {
    if (n <= 0) this
    else dec.dec(n - 1)
  }

  def print = println(count)
}