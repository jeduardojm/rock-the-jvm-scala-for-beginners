package part2poo

object MethodNotations extends App {

  class Person(val name: String, favoriteMovie:String, val age:Int = 0){
    def likes(movies:String):Boolean = movies == favoriteMovie
    def +(person:Person):String = s"${this.name} is hanging out with ${person.name}"
    def +(nickname:String):Person = new Person(s"$name ($nickname)",favoriteMovie)
    def unary_! : String = s"$name return anything"
    def unary_+ : Person = new Person(name,favoriteMovie,age +1)
    def isAlive: Boolean = true
    def apply(): String = s"Hi, my name is $name and I like $favoriteMovie"
    def apply(m : Int):String = s"$name is watched $favoriteMovie $m times"
    def learns(thing: String) = s"$name is learning $thing"
    def learScala = this learns "Scala"
  }

  val mary = new Person("Mary", "Inception")
  println(mary.likes("Inception"))
  println(mary likes "Inceptions")
  //infix notation = operator notation

  val tom = new Person("Tom","Fight Club")
  println(mary + tom)
  println(mary.+(tom))

  //prefix notation
  val x = -1
  val y = 1.unary_-
  //unary prefix onnly works with - + ~ !

  println(!mary)
  println(mary.unary_!)

  //posfix notation
  println(mary.isAlive)
  println(mary isAlive)

  //apply
  println(mary.apply())
  println(mary())

  println((mary + "the rockstart")())
  println((+mary).age)
  println(mary learScala)
  println(mary(10))
}
