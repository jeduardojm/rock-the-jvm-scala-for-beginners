package part2poo

object Objects extends App {

  // SCALA DOES NOT HAVE CLASS-LEVEL FUNCTIONALITY ("static")
  object Person{
    //"static"/"class" - level fucntionality
    val N_EYES = 2
    def canFly:Boolean = false

    //factor method
    def apply(mpther:Person, father:Person):Person = new Person("Booby")
  }
  class Person(val name:String){
    //instance-level functionality
  }

  //COMPANIONS
  println(Person.N_EYES)
  println(Person.canFly)

  //Scala object = SINGLETON INSTANCE
  val mary = new Person("Mary")
  val john = new Person("John")
  println(mary == john)


  val person1 = Person
  val person2 = Person
  println(person1 == person2)
}
